import { getUserFullName } from 'utils/user';
import { User } from 'interfaces';

describe('getFullName', () => {
  test('It should return `Jan Smidt` which is the supplied user`s first plus last name', () => {
    const user: User = {
      id: 1,
      email: 'jan@smi.dt',
      firstName: 'Jan',
      lastName: 'Smidt',
      role: {
        id: 1,
        level: 1,
        name: 'Admin',
      },
    };
    expect(getUserFullName(user)).toBe('Jan Smidt');
  });
});
