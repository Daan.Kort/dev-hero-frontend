import { getAdminPath, isAdmin, roleLevelMap } from 'utils/role';

describe('getAdminPath', () => {
  test('It should return `/admin/test` which is the supplied url with `/admin` prepended to it', () => {
    expect(getAdminPath('/test')).toBe('/admin/test');
  });

  test('It should return `/admin` which is the admin url without an appended path', () => {
    expect(getAdminPath()).toBe('/admin');
  });
});

describe('isAdmin', () => {
  test('It should return true because the supplied role level equals ALL_PERMISSIONS', () => {
    expect(isAdmin(1)).toBe(true);
  });

  test('It should return false because the supplied role level does not equal ALL_PERMISSIONS', () => {
    expect(isAdmin(0)).toBe(false);
  });
});
