import { Report } from 'interfaces';
import { isAssignmentAccepted } from 'utils/assignment';

describe('isAssignmentAccepted', () => {
  test('It should return true because the report array has at least one report with the status `ACCEPTED`', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
    ];
    expect(isAssignmentAccepted(reportArray)).toBe(true);
  });

  test('It should return false because the report array has no report with the status `ACCEPTED`', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
    ];
    expect(isAssignmentAccepted(reportArray)).toBe(false);
  });

  test('It should return false because an empty array has no report with the status `ACCEPTED`', () => {
    const reportArray: Report[] = [];
    expect(isAssignmentAccepted(reportArray)).toBe(false);
  });
});
