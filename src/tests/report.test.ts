import { Report } from 'interfaces';
import { getTotalPoints, getReportsByStatus } from 'utils/report';

describe('getTotalPoints', () => {
  test('It should return 2 because the sum of givenPoints of reports with the `ACCEPTED` status within the report array is 2', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
        pointsGiven: 1,
      },
      {
        id: 1,
        assignmentId: 2,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
        pointsGiven: 1,
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
        pointsGiven: 3,
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
    ];
    expect(getTotalPoints(reportArray)).toBe(2);
  });

  test('It should return 4 because the sum of givenPoints of reports on unique assignments with the `ACCEPTED` status within the report array is 4', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
        pointsGiven: 1,
      },
      {
        id: 1,
        assignmentId: 2,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
        pointsGiven: 1,
      },
      {
        id: 1,
        assignmentId: 2,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
        pointsGiven: 3,
      },
    ];
    expect(getTotalPoints(reportArray)).toBe(4);
  });

  test('It should return 0 because the array was empty', () => {
    const reportArray: Report[] = [];
    expect(getTotalPoints(reportArray)).toBe(0);
  });
});

describe('getReportsByStatus', () => {
  test('It should return an array of 2 reports with status `ACCEPTED`', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 2,
        assignmentId: 2,
        playerId: 2,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
    ];
    expect(getReportsByStatus(reportArray, 'ACCEPTED')).toStrictEqual([
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 2,
        assignmentId: 2,
        playerId: 2,
        answer: '',
        status: 'ACCEPTED',
      },
    ]);
  });

  test('It should return an array of 0 reports with status `REJECTED`', () => {
    const reportArray: Report[] = [
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 2,
        assignmentId: 2,
        playerId: 2,
        answer: '',
        status: 'ACCEPTED',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
      {
        id: 1,
        assignmentId: 1,
        playerId: 1,
        answer: '',
        status: 'REVIEW',
      },
    ];
    expect(getReportsByStatus(reportArray, 'REJECTED')).toStrictEqual([]);
  });

  test('It should return an empty array because an empty array was supplied', () => {
    const reportArray: Report[] = [];
    expect(getReportsByStatus(reportArray, 'REVIEW')).toStrictEqual([]);
  });
});
