import { getUserFullName } from 'utils/user';
import { unique } from 'utils/unique';
import { Report } from 'interfaces';
import { report } from 'process';

describe('unique', () => {
  test('It should return [1, 2, 3] because those are the only unique values in the given arrray', () => {
    expect([1, 1, 2, 3, 3].filter(unique)).toStrictEqual([1, 2, 3]);
  });

  test('It should return an empty array because an empty array was supplied', () => {
    expect([].filter(unique)).toStrictEqual([]);
  });

  test('It should return array with reports with id 1 and 2 because the second report with id 1 will be filtered out', () => {
    const report1: Report = {
      id: 1,
      assignmentId: 1,
      playerId: 1,
      answer: '',
      status: 'REVIEW',
      pointsGiven: 1,
    };
    const report2: Report = {
      id: 2,
      assignmentId: 2,
      playerId: 1,
      answer: '',
      status: 'ACCEPTED',
      pointsGiven: 3,
    };
    const reportArray: Report[] = [report1, report1, report1, report2];

    expect(reportArray.filter(unique)).toStrictEqual([report1, report2]);
  });
});
