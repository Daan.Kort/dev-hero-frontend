import { PlayerAssignment, Report } from 'interfaces';

export const isAssignmentAccepted = (assignment: PlayerAssignment): boolean =>
  assignment.reports.some((r) => r.status === 'ACCEPTED');

export const hasMaxPoints = (assignment: PlayerAssignment) => (
  reports: Report[],
) => {
  return (
    Math.max(...reports.map((report) => report.pointsGiven || 0), 0) ===
    assignment.pointsMaximum
  );
};
