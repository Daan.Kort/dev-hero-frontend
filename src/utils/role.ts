export const roleLevelMap = {
  PLAYER_PERMISSIONS: 0,
  ALL_PERMISSIONS: 1,
};

export type RoleLevel = keyof typeof roleLevelMap;

const adminPrefix = '/admin';

export const getAdminPath = (url = ''): string => {
  return adminPrefix + url;
};

export const isAdmin = (role?: number) => role === roleLevelMap.ALL_PERMISSIONS;
