import {
  Report,
  ReportStatus,
  HttpAssignment,
  HttpReport,
  PlayerReport,
} from 'interfaces';
import { unique } from './unique';

export const filterReportsByAssignment = (assignment: HttpAssignment) => (
  reports: HttpReport[],
) => reports.filter((report) => report.assignmentId === assignment.id);

const getAssignmentScores = (reports: Report[], assignmentId: number) =>
  reports.reduce(
    (arr: number[], report: Report) =>
      report.assignmentId === assignmentId && report.pointsGiven
        ? [report.pointsGiven, ...arr]
        : arr,
    [],
  );

export const getTotalPoints = (reports: PlayerReport[]): number => {
  const acceptedReports: PlayerReport[] = getReportsByStatus(
    reports,
    'ACCEPTED',
  );
  const assignmentIds: number[] = acceptedReports
    .map((report) => report.assignmentId)
    .filter(unique);
  const scores: number[] = assignmentIds.map((assignmentId) =>
    Math.max(...getAssignmentScores(acceptedReports, assignmentId)),
  );
  return scores.reduce((sum, score) => sum + score, 0);
};

export const getReportsByStatus = (
  reports: PlayerReport[],
  status: ReportStatus,
) => reports.filter((r: Report) => r.status === status);
