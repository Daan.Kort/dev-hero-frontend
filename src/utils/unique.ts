export const unique = (value: any, index: any, self: any) =>
  self.indexOf(value) === index;
