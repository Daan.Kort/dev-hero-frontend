import { User } from 'interfaces';

export const getUserFullName = (user: User) =>
  `${user.firstName} ${user.lastName}`;
