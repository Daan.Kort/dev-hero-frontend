import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import AppLayout from 'layouts/App';
import AdminLayout from 'layouts/Admin';
import PlayerLayout from 'layouts/Player';
import Docs from 'pages/General/Docs';
import AdminAssignmentList from 'pages/Admin/Assignments';
import PlayerAssignmentList from 'pages/Player/Assignments';
import CreateAssignment from 'pages/Admin/CreateAssignment';
import Login from 'pages/Login';
import ReportList from 'pages/Admin/ReportList';
import { getAdminPath } from 'utils/role';
import ProtectedRoute from 'components/ProtectedRoute';
import { useDispatch } from 'react-redux';
import { loadData } from 'redux/rootActions';

const App: React.FC = () => {
  const dispatch = useDispatch();

  const isUserLoggedIn: boolean = localStorage.state;
  if (isUserLoggedIn) {
    dispatch(loadData());
  }

  return (
    <BrowserRouter>
      <Switch>
        <ProtectedRoute path="/login">
          <Login />
        </ProtectedRoute>

        <Route path="/docs">
          <AppLayout>
            <Docs />
          </AppLayout>
        </Route>

        <ProtectedRoute path={getAdminPath('/:path?')} exact>
          <AdminLayout>
            <Switch>
              <Route exact path={getAdminPath('/')}>
                <AdminAssignmentList />
              </Route>
              <Route exact path={getAdminPath('/create-assignment')}>
                <CreateAssignment />
              </Route>
              <Route exact path={getAdminPath('/reports')}>
                <ReportList />
              </Route>
              <Redirect to={getAdminPath('/')} />
            </Switch>
          </AdminLayout>
        </ProtectedRoute>

        <ProtectedRoute>
          <PlayerLayout>
            <Switch>
              <Route exact path="/assignments/:status?">
                <PlayerAssignmentList />
              </Route>
              <Redirect to="/assignments" />
            </Switch>
          </PlayerLayout>
        </ProtectedRoute>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
