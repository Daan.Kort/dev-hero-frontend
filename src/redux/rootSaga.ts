import { all, call, takeEvery } from 'redux-saga/effects';
import userSaga, { getUsers, getUserData } from 'redux/actions/users/saga';
import assignmentsSaga, { getAssignments } from './actions/assignments/saga';
import reportsSaga, { getReports } from './actions/reports/saga';
import { isAdmin } from 'utils/role';
import { LOAD_DATA } from './rootActions';

export function* loadData() {
  const currentUser = yield call(getUserData);
  if (currentUser && isAdmin(currentUser.role.level)) {
    yield call(getUsers);
  }
  yield call(getAssignments);
  yield call(getReports);
}

export default function* rootSaga() {
  yield takeEvery(LOAD_DATA, loadData);
  yield all([userSaga(), assignmentsSaga(), reportsSaga()]);
}
