import { ActionType, createReducer } from 'typesafe-actions';
import * as actions from './actions';
import { User, ErrorResponse } from 'interfaces';

export interface UserState {
  token: string;
  error?: ErrorResponse;
  isLoading: boolean;
  success: boolean;
  currentUser: User | null;
  users: User[];
}

export type UserActions = ActionType<typeof actions>;

const initialState: UserState = {
  token: '',
  error: undefined,
  isLoading: false,
  success: false,
  currentUser: null,
  users: [],
};

export const userReducer = createReducer<UserState, UserActions>(initialState)
  .handleAction(actions.login, (state) => ({
    ...state,
    isLoading: true,
    success: false,
    error: initialState.error,
  }))
  .handleAction(actions.loginSuccess, (state, action) => ({
    ...state,
    success: true,
    isLoading: false,
    token: action.payload.token,
    error: initialState.error,
  }))
  .handleAction(actions.loginFail, (state, action) => ({
    ...state,
    success: false,
    isLoading: false,
    error: action.payload,
  }))
  .handleAction(actions.logout, (state) => ({
    ...state,
    token: '',
    currentUser: initialState.currentUser,
  }))
  .handleAction(actions.getUserData, (state) => ({
    ...state,
    isLoading: true,
    success: false,
    error: initialState.error,
  }))
  .handleAction(actions.userDataSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    success: true,
    currentUser: action.payload,
  }))
  .handleAction(actions.userDataFail, (state, action) => ({
    ...state,
    isLoading: false,
    success: false,
    error: action.payload,
  }))
  .handleAction(actions.fetchUsers, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: true,
    success: false,
  }))
  .handleAction(actions.fetchUsersSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    users: action.payload,
    success: true,
  }))
  .handleAction(actions.fetchUsersFailed, (state, action) => ({
    ...state,
    isLoading: false,
    success: false,
    error: action.payload,
  }));
