import { RootState } from 'redux/store';

export const selectUserState = (state: RootState) => state.users;

export const selectToken = (state: RootState) => selectUserState(state).token;

export const selectCurrentUser = (state: RootState) =>
  selectUserState(state).currentUser;

export const selectUserFullName = (state: RootState) => {
  const userState = selectUserState(state).currentUser;
  if (userState?.firstName && userState.lastName) {
    return `${userState?.firstName} ${userState?.lastName}`;
  } else {
    return '';
  }
};

export const selectUserRole = (state: RootState) =>
  selectCurrentUser(state)?.role;

export const selectUserRoleLevel = (state: RootState) =>
  selectUserRole(state)?.level;

export const selectUsers = (state: RootState) => selectUserState(state).users;

export const selectUserLoading = (state: RootState) =>
  selectUserState(state).isLoading;

export const selectUserError = (state: RootState) =>
  selectUserState(state).error;
