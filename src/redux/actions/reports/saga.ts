import { put, takeEvery, call } from 'redux-saga/effects';
import * as actionTypes from './action-types';
import * as actions from './actions';
import { ActionType } from 'typesafe-actions';
import {
  fetchReports,
  postReport,
  patchReport,
} from 'services/http/reportRequest';

export function* getReports() {
  try {
    const response = yield fetchReports();
    if (!response.error) {
      yield put({
        type: actionTypes.FETCH_REPORTS_SUCCESS,
        payload: response,
      });
    } else {
      yield put({
        type: actionTypes.FETCH_REPORTS_FAILED,
        payload: response,
      });
    }
  } catch (error) {
    yield put({
      type: actionTypes.FETCH_REPORTS_FAILED,
      payload: error,
    });
  }
}

function* createReport({ payload }: ActionType<typeof actions.postReport>) {
  try {
    const response = yield postReport(payload);
    if (!response.error) {
      yield put({
        type: actionTypes.POST_REPORT_SUCCESS,
      });
      yield call(getReports);
    } else {
      yield put({
        type: actionTypes.POST_REPORT_FAILED,
        payload: response.error,
      });
    }
  } catch (error) {
    yield put({
      type: actionTypes.POST_REPORT_FAILED,
      payload: error,
    });
  }
}

function* updateReport({ payload }: ActionType<typeof actions.patchReport>) {
  try {
    if (payload.id) {
      const response = yield patchReport(payload.id, payload);
      if (!response.error) {
        yield put({
          type: actionTypes.PATCH_REPORT_SUCCESS,
        });
        yield call(getReports);
      } else {
        yield put({
          type: actionTypes.PATCH_REPORT_FAILED,
          payload: response.error,
        });
      }
    } else {
      yield put({
        type: actionTypes.PATCH_REPORT_FAILED,
        payload: 'No id was provided',
      });
    }
  } catch (error) {
    yield put({
      type: actionTypes.PATCH_REPORT_FAILED,
      payload: error,
    });
  }
}

export default function* reportsSaga() {
  yield takeEvery(actionTypes.POST_REPORT, createReport);
  yield takeEvery(actionTypes.PATCH_REPORT, updateReport);
}
