import { createAction } from 'typesafe-actions';
import * as actionTypes from './action-types';
import { Report, ErrorResponse, ReportRequest } from 'interfaces';

export const fetchReports = createAction(actionTypes.FETCH_REPORTS)();
export const fetchReportsSuccess = createAction(
  actionTypes.FETCH_REPORTS_SUCCESS,
)<Report[]>();
export const fetchReportsFailed = createAction(
  actionTypes.FETCH_REPORTS_FAILED,
)<ErrorResponse>();

export const postReport = createAction(actionTypes.POST_REPORT)<
  ReportRequest
>();
export const postReportsSuccess = createAction(actionTypes.POST_REPORT_SUCCESS)<
  Report
>();
export const postReportsFailed = createAction(actionTypes.POST_REPORT_FAILED)<
  ErrorResponse
>();

export const patchReport = createAction(actionTypes.PATCH_REPORT)<
  Partial<Report>
>();
export const patchReportsSuccess = createAction(
  actionTypes.PATCH_REPORT_SUCCESS,
)<Report>();
export const patchReportsFailed = createAction(actionTypes.PATCH_REPORT_FAILED)<
  ErrorResponse
>();
