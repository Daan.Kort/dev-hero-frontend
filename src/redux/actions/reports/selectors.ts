import { RootState } from 'redux/store';

export const selectReportState = (state: RootState) => state.reports;

export const selectReports = (state: RootState) =>
  selectReportState(state).reports;

export const selectReportLoading = (state: RootState) =>
  selectReportState(state).isLoading;

export const selectReportError = (state: RootState) =>
  selectReportState(state).error;

export const selectPostReportSuccess = (state: RootState) =>
  selectReportState(state).success;

export const selectPatchReportLoading = (state: RootState) =>
  selectReportState(state).patchLoading;
