import { ActionType, createReducer } from 'typesafe-actions';
import * as actions from './actions';
import { ErrorResponse, PlayerReport, HttpReport } from 'interfaces';

export interface ReportState {
  reports: HttpReport[];
  error?: ErrorResponse;
  isLoading: boolean;
  patchLoading: boolean;
  success: boolean;
}
export interface PlayerReportState {
  reports: PlayerReport[];
  error?: ErrorResponse;
  isLoading: boolean;
  patchLoading: boolean;
  success: boolean;
}

export type ReportActions = ActionType<typeof actions>;

const initialState: ReportState = {
  reports: [],
  error: undefined,
  isLoading: true,
  patchLoading: false,
  success: false,
};

export const reportReducer = createReducer<ReportState, ReportActions>(
  initialState,
)
  .handleAction(actions.fetchReports, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: true,
    success: false,
  }))
  .handleAction(actions.fetchReportsSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    reports: action.payload,
    success: false,
    error: initialState.error,
  }))
  .handleAction(actions.fetchReportsFailed, (state, action) => ({
    ...state,
    isLoading: false,
    error: action.payload,
  }))
  .handleAction(actions.postReport, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: true,
    success: false,
  }))
  .handleAction(actions.postReportsSuccess, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: false,
    success: true,
  }))
  .handleAction(actions.postReportsFailed, (state, action) => ({
    ...state,
    error: action.payload,
    isLoading: false,
    success: false,
  }))
  .handleAction(actions.patchReport, (state) => ({
    ...state,
    error: initialState.error,
    patchLoading: true,
    success: false,
  }))
  .handleAction(actions.patchReportsSuccess, (state) => ({
    ...state,
    error: initialState.error,
    patchLoading: false,
    success: true,
  }))
  .handleAction(actions.patchReportsFailed, (state, action) => ({
    ...state,
    error: action.payload,
    patchLoading: false,
    success: false,
  }));
