import {
  AdminReport,
  HttpAssignment,
  HttpReport,
  HttpUser,
  PlayerAssignment,
  Report,
  ReportStatus,
} from 'interfaces';
import { converge, pipe } from 'rambda';
import { RootState } from 'redux/store';
import { hasMaxPoints, isAssignmentAccepted } from 'utils/assignment';
import { filterReportsByAssignment } from 'utils/report';
import { selectReports } from '../reports/selectors';
import { selectUsers } from '../users/selectors';

export const selectAssignmentState = (state: RootState) => state.assignments;

export const selectAssignments = pipe(
  selectAssignmentState,
  (state) => state.assignments,
);

export const selectPlayerAssignments: (
  state: RootState,
) => PlayerAssignment[] = converge(
  (assignments: HttpAssignment[], reports: HttpReport[]) =>
    assignments.map((assignment) => ({
      ...assignment,
      reports: filterReportsByAssignment(assignment)(reports),
    })),
  [selectAssignments, selectReports],
);

export const selectAdminAssignments: (
  state: RootState,
) => AdminReport[] = converge(
  (reports: HttpReport[], assignments: HttpAssignment[]) =>
    reports.map((report) => ({
      ...report,
      assignment: assignments.find(
        (assignment) => assignment.id === report.assignmentId,
      ),
    })),
  [selectReports, selectAssignments],
);

export const selectAdminReports = converge(
  (reports: HttpReport[], assignments: HttpAssignment[], users: HttpUser[]) =>
    reports.map((report) => ({
      ...report,
      player: users.find((user) => user.id === report.playerId),
      assignment: assignments.find(
        (assignment) => assignment.id === report.assignmentId,
      ),
    })),
  [selectReports, selectAssignments, selectUsers],
);

export const selectExtendedAdminReportReview = (status: ReportStatus) =>
  pipe(selectAdminReports, (reports) =>
    reports.filter((report: { status: string }) => report.status === status),
  );

export const selectAssignmentReports = (assignment: PlayerAssignment) =>
  pipe(selectReports, filterReportsByAssignment(assignment));

export const selectAssignmentLastReport = (assignment: PlayerAssignment) =>
  pipe(
    selectAssignmentReports(assignment),
    (assignmentReports): Report | null => assignmentReports.slice(-1)[0],
  );

export const selectAssignmentAnswer = (assignment: PlayerAssignment) => (
  state: RootState,
) => {
  const lastReport = selectAssignmentLastReport(assignment)(state);
  return lastReport?.answer;
};

export const selectAssignmentGivenPoints = (assignment: PlayerAssignment) => (
  state: RootState,
) => {
  const lastReport = selectAssignmentLastReport(assignment)(state);
  return lastReport?.pointsGiven || 0;
};

export const selectAssignmentStatus = (assignment: PlayerAssignment) => (
  state: RootState,
) => {
  const lastReport = selectAssignmentLastReport(assignment)(state);
  return lastReport?.status || 'OPEN';
};

export const selectAssignmentsWithStatus = (status: string) => (
  state: RootState,
) => {
  return selectPlayerAssignments(state).filter((assignment) => {
    return (
      (status === 'accepted' && isAssignmentAccepted(assignment)) ||
      (status !== 'accepted' &&
        selectIsAssignmentSubmittable(assignment)(state))
    );
  });
};

export const selectIsAssignmentSubmitted = (assignment: PlayerAssignment) => (
  state: RootState,
) => {
  return selectAssignmentReports(assignment)(state).length > 0;
};

export const selectIsAssignmentSubmittable = (assignment: PlayerAssignment) =>
  pipe(
    selectAssignmentReports(assignment),
    hasMaxPoints(assignment),
    (hasMaximum) => !hasMaximum,
  );

export const selectAssignmentLoading = (state: RootState) =>
  selectAssignmentState(state).isLoading;

export const selectAssignmentError = (state: RootState) =>
  selectAssignmentState(state).error;

export const selectPostAssignmentSuccess = (state: RootState) =>
  selectAssignmentState(state).success;
