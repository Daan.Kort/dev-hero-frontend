import { createAction } from 'typesafe-actions';
import * as actionTypes from './action-types';
import { HttpAssignment, ErrorResponse, AssignmentRequest } from 'interfaces';

export const fetchAssignments = createAction(actionTypes.FETCH_ASSIGNMENTS)();
export const fetchAssignmentsSuccess = createAction(
  actionTypes.FETCH_ASSIGNMENTS_SUCCESS,
)<HttpAssignment[]>();
export const fetchAssignmentsFailed = createAction(
  actionTypes.FETCH_ASSIGNMENTS_FAILED,
)<ErrorResponse>();

export const postAssignment = createAction(actionTypes.POST_ASSIGNMENT)<
  AssignmentRequest
>();
export const postAssignmentSuccess = createAction(
  actionTypes.POST_ASSIGNMENT_SUCCESS,
)<HttpAssignment>();
export const postAssignmentsFailed = createAction(
  actionTypes.POST_ASSIGNMENT_FAILED,
)<ErrorResponse>();
