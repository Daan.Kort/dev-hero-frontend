import { ActionType, createReducer } from 'typesafe-actions';
import * as actions from './actions';
import { HttpAssignment, ErrorResponse } from 'interfaces';

export interface AssignmentState {
  assignments: HttpAssignment[];
  error?: ErrorResponse;
  isLoading: boolean;
  success: boolean;
}

export type AssignmentActions = ActionType<typeof actions>;

const initialState: AssignmentState = {
  assignments: [],
  error: undefined,
  isLoading: true,
  success: false,
};

export const assignmentReducer = createReducer<
  AssignmentState,
  AssignmentActions
>(initialState)
  .handleAction(actions.fetchAssignments, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: true,
    success: false,
  }))
  .handleAction(actions.fetchAssignmentsSuccess, (state, action) => ({
    ...state,
    isLoading: false,
    assignments: action.payload,
    success: false,
    error: initialState.error,
  }))
  .handleAction(actions.fetchAssignmentsFailed, (state, action) => ({
    ...state,
    isLoading: false,
    error: action.payload,
  }))
  .handleAction(actions.postAssignment, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: true,
    success: false,
  }))
  .handleAction(actions.postAssignmentSuccess, (state) => ({
    ...state,
    error: initialState.error,
    isLoading: false,
    success: true,
  }))
  .handleAction(actions.postAssignmentsFailed, (state, action) => ({
    ...state,
    error: action.payload,
    isLoading: false,
    success: false,
  }));
