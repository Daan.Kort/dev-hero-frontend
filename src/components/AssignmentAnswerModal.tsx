import React, { useState, useEffect } from 'react';
import { PlayerAssignment } from 'interfaces';
import { useSelector } from 'react-redux';
import {
  selectReportError,
  selectPostReportSuccess,
} from 'redux/actions/reports/selectors';
import AssignmentModal from 'components/AssignmentModal';
import ModalCardBody from './bulma/components/Modal/ModalCardBody';
import ModalCardFooter from './bulma/components/Modal/ModalCardFooter';
import { selectAssignmentAnswer } from 'redux/actions/assignments/selectors';
import Label from './bulma/elements/Label';

interface Props {
  assignment: PlayerAssignment;
}

const CardBodyAnswer: React.FC<Props> = ({ assignment }) => {
  const error = useSelector(selectReportError);
  const success = useSelector(selectPostReportSuccess);
  const answer = useSelector(selectAssignmentAnswer(assignment));
  const [modalActive, setModalActive] = useState<boolean>(false);

  useEffect(() => {
    if (success) {
      setModalActive(false);
    }
  }, [success, setModalActive]);

  return (
    <>
      {modalActive && (
        <AssignmentModal
          assignment={assignment}
          closeFn={() => setModalActive(false)}
        >
          <ModalCardBody>
            <Label htmlFor="answer">Your Answer:</Label>
            <p>{answer}</p>
            {error && <p className="help">Something went wrong...</p>}
          </ModalCardBody>
          <ModalCardFooter />
        </AssignmentModal>
      )}
      {answer && (
        <button
          className="button is-small is-warning"
          style={{ marginTop: '20px' }}
          onClick={() => setModalActive(true)}
        >
          Show your answer
        </button>
      )}
    </>
  );
};

export default CardBodyAnswer;
