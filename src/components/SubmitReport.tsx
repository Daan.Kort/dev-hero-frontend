import React, { useState, useEffect } from 'react';
import { PlayerAssignment } from 'interfaces';
import { Form } from './bulma/form';
import AssignmentModal from './AssignmentModal';
import { ModalCardBody, ModalCardFooter } from './bulma/components';
import { Field, Label, Input } from './bulma/elements';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { postReport } from 'redux/actions/reports/actions';
import {
  selectReportLoading,
  selectReportError,
  selectPostReportSuccess,
} from 'redux/actions/reports/selectors';
import { useForm } from 'react-hook-form';

interface Props {
  assignment: PlayerAssignment;
  disabled: boolean;
}

type ReportRequest = { answer: string };

const SubmitReport: React.FC<Props> = ({ assignment, disabled }) => {
  const dispatch = useDispatch();
  const { register, errors, handleSubmit } = useForm<ReportRequest>();
  const isLoading = useSelector(selectReportLoading);
  const error = useSelector(selectReportError);
  const success = useSelector(selectPostReportSuccess);
  const [modalActive, setModalActive] = useState<boolean>(false);

  const handleSubmitReport = (formData: ReportRequest) => {
    dispatch(
      postReport({
        assignmentId: assignment.id,
        answer: formData.answer,
      }),
    );
  };

  useEffect(() => {
    if (success) {
      setModalActive(false);
    }
  }, [success, setModalActive]);

  return (
    <>
      {modalActive && (
        <AssignmentModal
          assignment={assignment}
          closeFn={() => setModalActive(false)}
        >
          <Form onSubmit={handleSubmit(handleSubmitReport)}>
            <ModalCardBody>
              <Field>
                <Label htmlFor="answer">Answer</Label>
                <Input
                  id="answer"
                  error={errors.answer}
                  name="answer"
                  ref={register()}
                />
              </Field>
              {error && <p className="help">Something went wrong...</p>}
            </ModalCardBody>
            <ModalCardFooter>
              <button
                type="submit"
                className={classnames('button is-info', {
                  'is-loading': isLoading,
                })}
              >
                Submit
              </button>
              <button
                type="button"
                className="button"
                onClick={() => setModalActive(false)}
              >
                Cancel
              </button>
            </ModalCardFooter>
          </Form>
        </AssignmentModal>
      )}
      <button
        className="button is-info"
        style={{ marginRight: 'auto' }}
        onClick={() => setModalActive(true)}
        disabled={disabled}
      >
        Submit this assignment
      </button>
    </>
  );
};

export default SubmitReport;
