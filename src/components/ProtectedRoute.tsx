import { Route, Redirect, useLocation } from 'react-router-dom';
import React from 'react';
import { useSelector } from 'react-redux';
import {
  selectUserState,
  selectUserRoleLevel,
} from 'redux/actions/users/selectors';
import { isAdmin, getAdminPath } from 'utils/role';

const ProtectedRoute: React.ReactType = ({ children, ...rest }) => {
  const token = useSelector(selectUserState).token;
  const isLoggedIn = !!token;
  const userRole = useSelector(selectUserRoleLevel);
  const isAdminRole = isAdmin(userRole);
  const { pathname } = useLocation();
  const isAdminPage = pathname.includes(getAdminPath());
  const isLoginPage = pathname.includes('login');
  const isUnauthorizedEntry = !isLoggedIn && !isLoginPage;

  if (isUnauthorizedEntry) {
    return <Redirect to="/login" />;
  }

  if (isLoggedIn) {
    if (isAdminRole && !isAdminPage) {
      return <Redirect to={getAdminPath()} />;
    }

    if (!isAdminRole && (isAdminPage || isLoginPage)) {
      return <Redirect to="/" />;
    }
  }

  return <Route {...rest}>{children}</Route>;
};

export default ProtectedRoute;
