import React from 'react';

type FormProps = {
  className?: string;
  onSubmit?: () => void;
};

const Form: React.FC<FormProps> = ({ className, children, onSubmit }) => {
  return (
    <form onSubmit={onSubmit} className={className}>
      {children}
    </form>
  );
};

export default Form;
