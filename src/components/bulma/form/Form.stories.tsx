import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import { Form, Control } from './index';
import { Label, Field, Box } from '../elements';

export default {
  title: 'Bulma/Form/Form',
  component: Form,
} as Meta;

const Template: Story = (args) => (
  <Box>
    <Form {...args} className="column is-one-third">
      <Field>
        <Label htmlFor="Name">Name</Label>
        <Control>
          <input
            className="input"
            type="text"
            placeholder="Name"
            onChange={(e) => {
              console.log(e.currentTarget.value);
            }}
          />
        </Control>
      </Field>
      <Field>
        <Label htmlFor="Desciption">Description</Label>
        <Control>
          <textarea
            style={{ resize: 'none' }}
            className="textarea"
            placeholder="Desciption"
          />
        </Control>
      </Field>
      <Field isGrouped>
        <Control>
          <button className="button">Submit</button>
        </Control>
        <Control>
          <button className="button">Cancel</button>
        </Control>
      </Field>
    </Form>
  </Box>
);

export const Default = Template.bind({});
Default.args = {};
