import React from 'react';

const ModalCardBody: React.FC = ({ children }) => (
  <section className="modal-card-body">{children}</section>
);

export default ModalCardBody;
