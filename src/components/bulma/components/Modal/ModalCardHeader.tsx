import React from 'react';

const ModalCardHeader: React.FC = ({ children }) => (
  <header className="modal-card-head">{children}</header>
);

export default ModalCardHeader;
