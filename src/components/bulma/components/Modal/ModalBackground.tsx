import React from 'react';

interface Props {
  onClick?: () => void;
}

const ModalBackground: React.FC<Props> = ({ onClick, children }) => (
  <div className="modal-background" onClick={onClick} />
);

export default ModalBackground;
