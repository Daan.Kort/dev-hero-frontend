import React from 'react';

const ModalCard: React.FC = ({ children }) => (
  <div className="modal-card">{children}</div>
);

export default ModalCard;
