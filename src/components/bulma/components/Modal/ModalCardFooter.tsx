import React from 'react';

const ModalCardFooter: React.FC = ({ children }) => (
  <footer className="modal-card-foot">{children}</footer>
);

export default ModalCardFooter;
