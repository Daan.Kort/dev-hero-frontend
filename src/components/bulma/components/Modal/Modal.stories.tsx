import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import {
  Modal,
  ModalBackground,
  ModalCard,
  ModalCardHeader,
  ModalCardTitle,
  ModalCardBody,
} from './';

export default {
  title: 'Bulma/Components/Modal',
  component: Modal,
} as Meta;

const Template: Story = (args) => (
  <Modal active>
    <ModalBackground />
    <ModalCard>
      <ModalCardHeader>
        <ModalCardTitle>example</ModalCardTitle>
        <button className="delete" aria-label="close" />
      </ModalCardHeader>
      <ModalCardBody>example</ModalCardBody>
    </ModalCard>
  </Modal>
);

export const Default = Template.bind({});
Default.args = {};
