import React from 'react';

const ModalCardTitle: React.FC = ({ children }) => (
  <p className="modal-card-title">{children}</p>
);

export default ModalCardTitle;
