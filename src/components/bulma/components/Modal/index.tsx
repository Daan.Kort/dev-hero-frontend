import React from 'react';
import ModalBackground from './ModalBackground';
import ModalCard from './ModalCard';
import ModalCardHeader from './ModalCardHeader';
import ModalCardTitle from './ModalCardTitle';
import ModalCardBody from './ModalCardBody';
import ModalCardFooter from './ModalCardFooter';
import classnames from 'classnames';

interface Props {
  active?: boolean;
}

const Modal: React.FC<Props> = ({ active, children }) => (
  <div className={classnames('modal', { 'is-active': active })}>{children}</div>
);

export {
  Modal,
  ModalBackground,
  ModalCard,
  ModalCardHeader,
  ModalCardTitle,
  ModalCardBody,
  ModalCardFooter,
};
