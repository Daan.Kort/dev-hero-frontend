import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import {
  Card,
  CardImage,
  CardHeader,
  CardHeaderTitle,
  CardHeaderIcon,
  CardContent,
  CardFooter,
  CardFooterItem,
} from './';

export default {
  title: 'Bulma/Components/Card',
  component: Card,
} as Meta;

const Template: Story = (args) => (
  <Card>
    <CardImage>
      <img
        src="https://bulma.io/images/placeholders/1280x960.png"
        alt="card placeholder"
      />
    </CardImage>
    <CardHeader>
      <CardHeaderTitle>Card Title</CardHeaderTitle>
      <CardHeaderIcon>Card Icon</CardHeaderIcon>
    </CardHeader>
    <CardContent> Card Content</CardContent>
    <CardFooter>
      <CardFooterItem>Card footer item 1</CardFooterItem>
      <CardFooterItem>
        <span>Card footer item 2</span>
      </CardFooterItem>
    </CardFooter>
  </Card>
);

export const Default = Template.bind({});
Default.args = {};
