import React from 'react';

const CardFooterItem: React.FC = ({ children }) => (
  <div className="card-footer-item">{children}</div>
);

export default CardFooterItem;
