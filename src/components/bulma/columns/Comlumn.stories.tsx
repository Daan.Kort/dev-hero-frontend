import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Columns, Column } from './';
import Box from '../elements/Box';

export default {
  title: 'Bulma/Columns/Column',
  component: Columns,
} as Meta;

const Template: Story = (args) => (
  <>
    <Columns desktop>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
    </Columns>
    <Columns desktop>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
    </Columns>
    <Columns desktop>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
      <Column {...args}>
        <Box>example sizeDesktop 6</Box>
      </Column>
    </Columns>
  </>
);

export const Default = Template.bind({});
Default.args = {
  sizeDesktop: 6,
};
