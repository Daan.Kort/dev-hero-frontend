import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Tag from './Tag';

export default {
  title: 'Bulma/Elements/Tags',
  component: Tag,
} as Meta;

const Template: Story = (args) => (
  <Tag {...args}>
    <p>Simple content</p>
  </Tag>
);

export const Default = Template.bind({});
Default.args = {
  onClick: false,
};
