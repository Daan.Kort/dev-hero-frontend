import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Label from './';

export default {
  title: 'Bulma/Elements/Label',
  component: Label,
} as Meta;

const Template: Story = (args) => (
  <Label htmlFor="example" {...args}>
    <p>Simple label</p>
  </Label>
);

export const Default = Template.bind({});
Default.args = {};
