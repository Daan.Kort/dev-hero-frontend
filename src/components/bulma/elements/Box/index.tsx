import React from 'react';

export interface BoxProps {
  /**
   * If box with tabs, it sets border top radius to 0
   */
  withTabs?: boolean;
}

const Box: React.FC<BoxProps> = ({ withTabs = false, children }) => (
  <div
    className="box"
    style={withTabs ? { borderTopLeftRadius: 0, borderTopRightRadius: 0 } : {}}
  >
    {children}
  </div>
);

export default Box;
