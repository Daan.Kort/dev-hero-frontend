import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import IconContainer from './';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

export default {
  title: 'Bulma/Elements/IconContainer',
  component: IconContainer,
} as Meta;

const Template: Story = (args) => (
  <IconContainer>
    <FontAwesomeIcon size="2x" icon={faPlusCircle} />
  </IconContainer>
);

export const Default = Template.bind({});
Default.args = {};
