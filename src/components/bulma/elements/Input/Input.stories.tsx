import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Input from './';

export default {
  title: 'Bulma/Elements/Input',
  component: Input,
} as Meta;

const Template: Story = (args) => (
  <Input
    {...args}
    id="title"
    name="title"
    type="text"
    placeholder="this is a example"
  />
);

export const Default = Template.bind({});
Default.args = {};
