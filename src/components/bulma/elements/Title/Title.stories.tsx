import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Title, { TitleProps } from './';

export default {
  title: 'Bulma/Elements/Title',
  component: Title,
} as Meta;

const TemplateH1: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h1</Title>
);
export const Heading1 = TemplateH1.bind({});
Heading1.args = {
  as: 'h1',
  styledAs: 'h1',
};

const TemplateH2: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h2</Title>
);
export const Heading2 = TemplateH2.bind({});
Heading2.args = {
  as: 'h2',
  styledAs: 'h2',
};

const TemplateH3: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h3</Title>
);
export const Heading3 = TemplateH3.bind({});
Heading3.args = {
  as: 'h3',
  styledAs: 'h3',
};

const TemplateH4: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h4</Title>
);
export const Heading4 = TemplateH4.bind({});
Heading4.args = {
  as: 'h4',
  styledAs: 'h4',
};

const TemplateH5: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h5</Title>
);
export const Heading5 = TemplateH5.bind({});
Heading5.args = {
  as: 'h5',
  styledAs: 'h5',
};

const TemplateH6: Story<TitleProps> = (args) => (
  <Title {...args}>Heading h6</Title>
);
export const Heading6 = TemplateH6.bind({});
Heading6.args = {
  as: 'h6',
  styledAs: 'h6',
};
