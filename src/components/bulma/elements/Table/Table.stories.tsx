import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Table from './';

export default {
  title: 'Bulma/Elements/Table',
  component: Table,
} as Meta;

const Template: Story = (args) => (
  <Table hoverable striped fullWidth>
    <thead>
      <Table.Row>
        <th>Name</th>
        <th>Title</th>
        <th>Answer</th>
        <th align="center">Points</th>
        <th align="center">Review</th>
      </Table.Row>
    </thead>
    <tbody>example</tbody>
  </Table>
);

export const Default = Template.bind({});
Default.args = {};
