import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Field from './';
import { Control } from 'components/bulma/form';

export default {
  title: 'Bulma/Elements/Fields',
  component: Field,
} as Meta;

const Template: Story = (args) => (
  <>
    <p>These are 2 examples for field: </p>
    <p>This is where you have 2 button in the field.</p>
    <Field isGrouped>
      <Control>
        <button className="button">is grouped, and a button</button>
      </Control>
      <Control>
        <button className="button">is grouped, and a button</button>
      </Control>
    </Field>
    <p>This is where you got an input with a button</p>
    <Field hasAddons>
      <Control>
        <input className="input"></input>
      </Control>
      <Control>
        <button className="button">Has addons</button>
      </Control>
    </Field>
  </>
);

export const Default = Template.bind({});
Default.args = {};
