import React, { FC } from 'react';
import classNames from 'classnames';
import { FieldError } from 'react-hook-form';

export interface ErrorProps {
  /**
   * We're using just error type that can be string or required|min|max|minLength|maxLength|pattern + message
   */
  error?: FieldError;
  /**
   * Usually we need a custom class name because component is not really capable of rendering itself
   */
  className?: string;
}

const showErrorMessage = (error: FieldError) => {
  switch (error.type) {
    case 'required':
      return 'Field is required.';
    case 'min':
      return 'Your input required more characters';
    case 'minLength':
      return 'Your input required more characters';
    case 'maxLength':
      return 'Your input exceed maxLength';
    case 'max':
      return 'Your input exceed maxLength';
    case 'pattern':
      return error.message;
    default:
      return '';
  }
};

const Error: FC<ErrorProps> = ({ error, className }) => (
  <p className={classNames('help is-danger', className)}>
    {error && showErrorMessage(error)}
  </p>
);

export default Error;
