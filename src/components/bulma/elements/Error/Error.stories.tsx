import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Error, { ErrorProps } from './';

export default {
  title: 'Bulma/Elements/Error',
  component: Error,
} as Meta;

const Template: Story<ErrorProps> = (args) => <Error {...args} />;

export const Default = Template.bind({});
Default.args = {
  error: {
    type: 'required',
  },
};

export const CustomMessage = Template.bind({});
CustomMessage.args = {
  error: {
    type: 'pattern',
    message: 'In case of type pattern, we show error message',
  },
};
