import React, { useState } from 'react';
import { ExtendedAdminReport } from 'interfaces';
import { Table, Field, IconContainer } from './bulma/elements';
import { getUserFullName } from 'utils/user';
import { Control } from './bulma/form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';
import { patchReport } from 'redux/actions/reports/actions';

interface Props {
  report: ExtendedAdminReport;
}

const ReportRow: React.FC<Props> = ({ report }) => {
  const verticalAlignStyle: React.CSSProperties = { verticalAlign: 'middle' };
  const buttonNoStyle: React.CSSProperties = {
    background: 'none',
    border: 'none',
  };
  const dispatch = useDispatch();
  const [pointsGiven, setPointsGiven] = useState<number>(
    report.assignment.pointsMaximum,
  );

  const approve = () => {
    dispatch(
      patchReport({
        id: report.id,
        assignmentId: report.assignment.id,
        status: 'ACCEPTED',
        pointsGiven: pointsGiven,
      }),
    );
  };

  const reject = () => {
    dispatch(
      patchReport({
        id: report.id,
        assignmentId: report.assignment.id,
        status: 'REJECTED',
      }),
    );
  };

  return (
    <Table.Row>
      <td style={verticalAlignStyle}>{getUserFullName(report.player)}</td>
      <td style={verticalAlignStyle}>{report.assignment.title}</td>
      <td style={verticalAlignStyle}>{report.assignment.topic}</td>
      <td style={verticalAlignStyle}>
        {report.answer || 'No answer provided..'}
      </td>
      <td align="center">
        <div className="select">
          <select
            value={pointsGiven}
            onChange={(e) => setPointsGiven(+e.currentTarget.value)}
          >
            {Array(report.assignment.pointsMaximum)
              .fill(0)
              .map((o, i) => i + 1)
              .map((n: number) => (
                <option key={n} value={n}>
                  {n}
                </option>
              ))}
          </select>
        </div>
      </td>
      <td align="center" style={verticalAlignStyle}>
        <Field isGrouped groupCentered>
          <Control>
            <IconContainer>
              <button
                className="button has-text-success"
                style={buttonNoStyle}
                onClick={(e) => approve()}
                title="Accept"
              >
                <FontAwesomeIcon icon={faCheck} />
              </button>
            </IconContainer>
          </Control>
          <Control>
            <IconContainer>
              <button
                className="button has-text-danger"
                style={buttonNoStyle}
                onClick={(e) => reject()}
                title="Reject"
              >
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </IconContainer>
          </Control>
        </Field>
      </td>
    </Table.Row>
  );
};

export default ReportRow;
