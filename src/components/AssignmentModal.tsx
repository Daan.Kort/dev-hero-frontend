import React from 'react';
import {
  Modal,
  ModalBackground,
  ModalCard,
  ModalCardHeader,
  ModalCardTitle,
  ModalCardBody,
} from 'components/bulma/components';
import { PlayerAssignment, Report } from 'interfaces';

type AssignmentProps = {
  assignment: PlayerAssignment;
  report?: Report;
  closeFn: () => void;
};

const AssignmentModal: React.FC<AssignmentProps> = ({
  children,
  assignment,
  closeFn,
}) => (
  <Modal active>
    <ModalBackground onClick={closeFn} />
    <ModalCard>
      <ModalCardHeader>
        <ModalCardTitle>{assignment.title}</ModalCardTitle>
        <button className="delete" aria-label="close" onClick={closeFn} />
      </ModalCardHeader>
      <ModalCardBody>{assignment.description}</ModalCardBody>
      {children}
    </ModalCard>
  </Modal>
);

export default AssignmentModal;
