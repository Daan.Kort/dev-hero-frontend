import React from 'react';
import { useDispatch } from 'react-redux';
import { logout } from 'redux/actions/users/actions';

const LogoutButton: React.FC = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <div className="navbar-item">
      <div className="buttons">
        <button onClick={handleLogout} className="button is-primary">
          Logout
        </button>
      </div>
    </div>
  );
};

export default LogoutButton;
