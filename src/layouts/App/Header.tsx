import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import logo from './logo.svg';
import classnames from 'classnames';
import { Tag } from 'components/bulma/elements';
import { useSelector } from 'react-redux';
import {
  selectUserRoleLevel,
  selectUserFullName,
} from 'redux/actions/users/selectors';
import { getAdminPath } from 'utils/role';
import { isAdmin } from 'utils/role';

const Header: React.FC = ({ children }) => {
  const [isMenuActive, setIsMenuActive] = useState(false);
  const userRole = useSelector(selectUserRoleLevel);
  const fullName = useSelector(selectUserFullName);

  return (
    <header>
      <nav
        className="navbar is-dark"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <NavLink
            className="navbar-item is-family-code"
            to={isAdmin(userRole) ? getAdminPath('/') : '/'}
          >
            <img src={logo} alt="Dev Hero" />
            <h1 className="is-sr-only">Dev Hero</h1>
            {fullName && <span className="mr-2">{fullName}</span>}
            {typeof userRole !== 'undefined' && (
              <Tag color={isAdmin(userRole) ? 'danger' : 'info'}>
                {isAdmin(userRole) ? 'Admin' : 'Player'}
              </Tag>
            )}
          </NavLink>
          <button
            className={classnames('navbar-burger burger', {
              'is-active': isMenuActive,
            })}
            style={{
              border: 'none',
              outline: 'none',
              backgroundColor: 'unset',
            }}
            onClick={() => setIsMenuActive(!isMenuActive)}
            aria-label="menu"
            aria-expanded={isMenuActive}
            data-target="navMenu"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>
        <div
          className={classnames('navbar-menu', { 'is-active': isMenuActive })}
          id="navMenu"
          style={{ backgroundColor: 'unset' }}
        >
          {children}
        </div>
      </nav>
    </header>
  );
};

export default Header;
