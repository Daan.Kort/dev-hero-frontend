import React from 'react';
import Header from './Header';
import LayoutContent from 'layouts/Content';

const AppLayout: React.FC = ({ children }) => {
  return (
    <>
      <Header />
      <LayoutContent>{children}</LayoutContent>
    </>
  );
};

export default AppLayout;
