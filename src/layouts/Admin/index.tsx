import React from 'react';
import AdminHeader from './Header';
import LayoutContent from 'layouts/Content';

const AdminLayout: React.FC = ({ children }) => {
  return (
    <>
      <AdminHeader />
      <LayoutContent>{children}</LayoutContent>
    </>
  );
};

export default AdminLayout;
