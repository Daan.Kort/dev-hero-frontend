import React from 'react';
import Header from 'layouts/App/Header';
import { NavLink } from 'react-router-dom';
import { getAdminPath } from 'utils/role';
import LogoutButton from 'layouts/LogoutButton';

const AdminHeader: React.FC = () => (
  <Header>
    <div className="navbar-start">
      <NavLink className="navbar-item" to={getAdminPath('/reports')}>
        Reports
      </NavLink>
    </div>
    <div className="navbar-end">
      <LogoutButton />
    </div>
  </Header>
);

export default AdminHeader;
