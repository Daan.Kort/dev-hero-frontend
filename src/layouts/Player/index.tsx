import React from 'react';
import { Tabs, Tab } from 'components/bulma/components';
import { Box } from 'components/bulma/elements';
import Header from '../App/Header';
import LayoutContent from 'layouts/Content';
import LogoutButton from 'layouts/LogoutButton';
import {
  selectReportLoading,
  selectReportError,
  selectReports,
} from 'redux/actions/reports/selectors';
import { useSelector } from 'react-redux';
import { getTotalPoints } from 'utils/report';

const PlayerLayout: React.FC = ({ children }) => {
  const reports = useSelector(selectReports);
  const isLoading = useSelector(selectReportLoading);
  const error = useSelector(selectReportError);

  return (
    <>
      <Header>
        <div className="navbar-end">
          {!isLoading && !error && (
            <div className="navbar-item">
              Total points: {getTotalPoints(reports)}
            </div>
          )}
          <LogoutButton />
        </div>
      </Header>
      <LayoutContent>
        <Tabs>
          <Tab to="/assignments">Assignments</Tab>
          <Tab to="/assignments/accepted">Accepted assignments</Tab>
        </Tabs>
        <Box withTabs>{children}</Box>
      </LayoutContent>
    </>
  );
};

export default PlayerLayout;
