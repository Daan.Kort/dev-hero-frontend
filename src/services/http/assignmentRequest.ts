import fetchApi, { defaultHeaders, authHeader } from 'utils/fetchApi';
import { HttpAssignment, ErrorResponse, AssignmentRequest } from 'interfaces';

export const fetchAssignments = () => {
  return fetchApi<HttpAssignment[] | ErrorResponse>('/assignments', {
    method: 'GET',
    headers: {
      ...defaultHeaders,
      ...authHeader(),
    },
  });
};

export const postAssignments = (assignmentData: AssignmentRequest) => {
  return fetchApi<HttpAssignment[] | ErrorResponse>('/assignments', {
    method: 'POST',
    headers: {
      ...defaultHeaders,
      ...authHeader(),
    },
    data: {
      ...assignmentData,
    },
  });
};
