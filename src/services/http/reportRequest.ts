import fetchApi, { defaultHeaders, authHeader } from 'utils/fetchApi';
import { Report, ErrorResponse, ReportRequest } from 'interfaces';

export const fetchReports = () => {
  return fetchApi<Report[] | ErrorResponse>('/reports', {
    method: 'GET',
    headers: {
      ...defaultHeaders,
      ...authHeader(),
    },
  });
};

export const postReport = (reportData: ReportRequest) => {
  return fetchApi<Report[] | ErrorResponse>('/reports', {
    method: 'POST',
    headers: {
      ...defaultHeaders,
      ...authHeader(),
    },
    data: {
      ...reportData,
    },
  });
};

export const patchReport = (id: number, reportData: Partial<Report>) => {
  return fetchApi<Report[] | ErrorResponse>(`/reports/${id}`, {
    method: 'PATCH',
    headers: {
      ...defaultHeaders,
      ...authHeader(),
    },
    data: {
      ...reportData,
    },
  });
};
