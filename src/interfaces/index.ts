export interface PlayerAssignment {
  id: number;
  title: string;
  description: string;
  topic: string;
  pointsMaximum: number;
  reports: PlayerReport[];
}
export interface AdminReport {
  id: number;
  player: User;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
  assignment: AdminAssignment[];
}

export interface AdminAssignment {
  id: number;
  title: string;
  description: string;
  topic: string;
  pointsMaximum: number;
}

export interface PlayerReport {
  id: number;
  assignmentId: number;
  playerId: number;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
}

export interface HttpAssignment {
  id: number;
  title: string;
  description: string;
  topic: string;
  pointsMaximum: number;
}

export interface ExtendedAdminReport {
  id: number;
  assignment: HttpAssignment;
  player: User;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
}

export interface HttpReport {
  id: number;
  assignmentId: number;
  playerId: number;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
}

export interface HttpUser {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  role: UserRole;
}

export type AssignmentRequest = Omit<HttpAssignment, 'id'>;

export type ReportStatus = 'OPEN' | 'REVIEW' | 'ACCEPTED' | 'REJECTED';

export interface Report {
  id: number;
  assignmentId: number;
  playerId: number;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
}

export interface ExtendedReport {
  id: number;
  assignment: HttpAssignment;
  player: User;
  answer: string;
  status: ReportStatus;
  pointsGiven?: number;
}

export type ReportRequest = {
  assignmentId: number;
  answer: string;
};

export interface UserAuth {
  email: string;
}

export interface LoginResponse {
  token: string;
}

export interface ErrorResponse {
  code: number;
  message: string;
}

export interface UserRole {
  id: number;
  name: string;
  level: number;
}

export interface User {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  role: UserRole;
}

export interface ParamTypes {
  status: string;
}
