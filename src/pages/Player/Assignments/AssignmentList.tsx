import React from 'react';
import { ParamTypes, PlayerAssignment } from 'interfaces';
import AssignmentCard from 'pages/Player/Assignments/AssignmentCard';
import { Columns } from 'components/bulma/columns';
import {
  selectAssignmentLoading,
  selectAssignmentError,
  selectAssignmentsWithStatus,
} from 'redux/actions/assignments/selectors';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const AssignmentList: React.FC = () => {
  const { status } = useParams<ParamTypes>();
  const assignmentsWithStatus = useSelector(
    selectAssignmentsWithStatus(status),
  );
  const assignmentsLoading = useSelector(selectAssignmentLoading);
  const assignmentsError = useSelector(selectAssignmentError);

  if (assignmentsLoading) {
    return <p>Loading...</p>;
  }
  if (assignmentsError) {
    return <p>Something went wrong...</p>;
  }
  if (!assignmentsWithStatus.length) {
    return <p>No assignments yet</p>;
  }

  return (
    <Columns gap={8} multiline>
      {assignmentsWithStatus.map((assignment: PlayerAssignment) => (
        <AssignmentCard key={assignment.id} assignment={assignment} />
      ))}
    </Columns>
  );
};

export default AssignmentList;
