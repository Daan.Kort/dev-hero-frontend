import React from 'react';
import AssignmentList from 'pages/Player/Assignments/AssignmentList';

const PlayerAssignmentList: React.FC = () => <AssignmentList />;

export default PlayerAssignmentList;
