import React from 'react';
import {
  Card,
  CardHeader,
  CardHeaderTitle,
  CardHeaderIcon,
  CardContent,
  CardFooter,
  CardFooterItem,
} from 'components/bulma/components';
import { Tag } from 'components/bulma/elements/Tags';
import { Column } from '../../../components/bulma/columns';
import { PlayerAssignment } from 'interfaces';
import SubmitReport from '../../../components/SubmitReport';
import { useSelector } from 'react-redux';
import {
  selectAssignmentGivenPoints,
  selectAssignmentStatus,
  selectIsAssignmentSubmittable,
} from 'redux/actions/assignments/selectors';
import CardBodyAnswer from 'components/AssignmentAnswerModal';

const assignmentStatusMap = {
  OPEN: 'info',
  REVIEW: 'info',
  ACCEPTED: 'success',
  REJECTED: 'danger',
} as const;

const AssignmentCard: React.FC<{
  assignment: PlayerAssignment;
}> = ({ assignment }) => {
  const isAssignmentSubmittable = useSelector(
    selectIsAssignmentSubmittable(assignment),
  );
  const givenPoints = useSelector(selectAssignmentGivenPoints(assignment));
  const status = useSelector(selectAssignmentStatus(assignment));
  const givenPointsLabel =
    status === 'ACCEPTED'
      ? givenPoints + '/' + assignment.pointsMaximum
      : assignment.pointsMaximum;
  return (
    <Column size={4}>
      <Card>
        <CardHeader>
          <CardHeaderTitle>
            <span>{assignment.title}</span>
            {isAssignmentSubmittable && (
              <>
                <Tag
                  className="ml-5 is-capitalized"
                  color={assignmentStatusMap[status]}
                >
                  {status.toLowerCase()}
                </Tag>
                <Tag
                  className="ml-5 is-capitalized"
                  color={assignmentStatusMap.OPEN}
                >
                  {assignment.topic}
                </Tag>
              </>
            )}
          </CardHeaderTitle>
          <CardHeaderIcon>
            {givenPointsLabel}
            pt
          </CardHeaderIcon>
        </CardHeader>
        <CardContent>
          <p style={{ whiteSpace: 'pre-wrap' }}>{assignment.description}</p>
          <CardBodyAnswer assignment={assignment} />
        </CardContent>
        {isAssignmentSubmittable && (
          <CardFooter>
            <CardFooterItem>
              <SubmitReport
                assignment={assignment}
                disabled={status === 'REVIEW'}
              />
            </CardFooterItem>
          </CardFooter>
        )}
      </Card>
    </Column>
  );
};

export default AssignmentCard;
