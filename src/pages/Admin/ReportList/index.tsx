import React from 'react';
import { Title, Table } from 'components/bulma/elements';
import { ExtendedReport } from 'interfaces';
import {
  selectAssignmentLoading,
  selectAssignmentError,
  selectExtendedAdminReportReview,
} from 'redux/actions/assignments/selectors';
import {
  selectReportLoading,
  selectReportError,
} from 'redux/actions/reports/selectors';
import { useSelector } from 'react-redux';
import ReportRow from 'components/ReportRow';

const ReportList: React.FC = () => {
  const isLoading = useSelector(selectReportLoading || selectAssignmentLoading);
  const error = useSelector(selectReportError || selectAssignmentError);
  const reports = useSelector(selectExtendedAdminReportReview('REVIEW'));

  if (isLoading) {
    return <p>Loading...</p>;
  }
  if (error) {
    return <p className="help">Something went wrong...</p>;
  }
  return (
    <>
      <Title>Reports</Title>
      {reports === 0 ? (
        <p>No reports to review..</p>
      ) : (
        <Table hoverable striped fullWidth>
          <thead>
            <Table.Row>
              <th>Name</th>
              <th>Title</th>
              <th>Topic</th>
              <th>Answer</th>
              <th align="center">Points</th>
              <th align="center">Review</th>
            </Table.Row>
          </thead>
          <tbody>
            {reports.map((r: ExtendedReport, i: number) => (
              <ReportRow key={i} report={r} />
            ))}
          </tbody>
        </Table>
      )}
    </>
  );
};

export default ReportList;
