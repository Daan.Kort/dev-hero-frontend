import React from 'react';
import { AdminAssignment } from 'interfaces';
import { Columns } from 'components/bulma/columns';
import {
  selectAssignments,
  selectAssignmentLoading,
  selectAssignmentError,
} from 'redux/actions/assignments/selectors';
import { useSelector } from 'react-redux';
import AssignmentCard from './AssignmentCard';

const AssignmentList: React.FC = () => {
  const assignments = useSelector(selectAssignments);
  const assignmentsLoading = useSelector(selectAssignmentLoading);
  const assignmentsError = useSelector(selectAssignmentError);

  if (assignmentsLoading) {
    return <p>Loading...</p>;
  }
  if (assignmentsError) {
    return <p>Something went wrong...</p>;
  }
  if (!assignments.length) {
    return <p>No assignment yet</p>;
  }

  return (
    <Columns gap={8} multiline>
      {assignments.map((assignment: AdminAssignment) => (
        <AssignmentCard key={assignment.id} assignment={assignment} />
      ))}
    </Columns>
  );
};

export default AssignmentList;
