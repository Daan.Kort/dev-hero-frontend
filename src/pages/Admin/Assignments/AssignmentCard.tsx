import React from 'react';
import {
  Card,
  CardHeader,
  CardHeaderTitle,
  CardHeaderIcon,
  CardContent,
} from 'components/bulma/components';
import { Column } from 'components/bulma/columns';
import { AdminAssignment } from 'interfaces';

const AssignmentCard: React.FC<{
  assignment: AdminAssignment;
}> = ({ assignment }) => {
  return (
    <Column size={4}>
      <Card>
        <CardHeader>
          <CardHeaderTitle>
            <span>{assignment.title}</span>
          </CardHeaderTitle>
          <CardHeaderIcon>{assignment.pointsMaximum} pt</CardHeaderIcon>
        </CardHeader>
        <CardContent>
          <p style={{ whiteSpace: 'pre-wrap' }}>{assignment.description}</p>
        </CardContent>
      </Card>
    </Column>
  );
};

export default AssignmentCard;
