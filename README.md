# Dev Hero

Dev Hero is an application where developers are able to train their development skills through gamification. Developers are able to take assignments which when submitted will be reviewed and rewarded with points based on how well the assignment was executed.

## Roles

Currently there are 2 available roles, player and admin

Players: A player is able to take and submit assignemnts listed in the assignment list created by an admin

Admin: An admin is able to create and review submitted assignments by players as well as creating reports for submitted assignments


# Dev Hero React Frontend

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[Bulma theme doc](https://jenil.github.io/bulmaswatch/slate/)

## Storybook

### `npm run storybook`

We have UI documentation handled by [Storybook](https://storybook.js.org/). By running this command, you can access components collection.
Feel free to extend :fire:

## Available Scripts

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br />

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

### env usage 
copy `.env.example` into `.env` and fill in the right values
